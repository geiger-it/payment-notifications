#!/bin/bash

sudo apt-get update

sudo apt-get -y install nginx
sudo apt-get -y install php7.0-fpm php7.0-common php7.0-cli php7.0-mysqlnd php7.0-curl php7.0-intl \
php7.0-mcrypt php7.0-json php7.0-mbstring php7.0-xml php7.0-gd php7.0-odbc php7.0-bcmath php7.0-zip

# AS400 access
sudo apt-get install unixodbc
# wget from://somewhere/ibm-iaccess-1.1.0.1-1.0.amd64.deb
sudo dpkg -i ibm-iaccess-1.1.0.1-1.0.amd64.deb

# cron scheduler for Laravel
(crontab -l ; echo "* * * * * php /home/ubuntu/projects/payment-notifications/artisan schedule:run >> /dev/null 2>&1") | crontab -

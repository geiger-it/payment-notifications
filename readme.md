## Payment Notifications

#### Purpose
What's it doing?

#### Installation
  - git clone
  - composer install
  - provision.sh

#### Notes
  - Cron runs updater command `app\Console\Commands\Updater` to query for x number of payment decline types and sends out either a first or second attempt email.

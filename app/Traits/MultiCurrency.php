<?php

namespace App\Traits;

trait MultiCurrency {


    public function customerCurrency($value, $currency) {
        if($currency) {
            return round($value * $currency->exchangeRate, isset($currency->currencyMaster->decimalNumberForAmountDisplay) ? $currency->currencyMaster->decimalNumberForAmountDisplay : 2);
        }
        return $value;
    }

    public function customerCurrencyName($currency) {
        if($currency) {
            return $currency->customerCurrency;
        }
        return;
    }

}
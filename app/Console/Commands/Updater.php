<?php

/**
 * Created by PhpStorm.
 * User: dharrington
 * Date: 11/9/2017
 * Time: 9:33 AM
 */

namespace App\Console\Commands;

use App\CommandRunner;
use App\Http\Models\DeclinedRecord;
use App\Http\Models\Z1OORSHEM;
use App\Mail\EmailDailyReport;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;


class Updater extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'updater:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run all commands daily at 9 a.m';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {


        $start_time = Carbon::now();

        $email_success_count = 0;
        $email_failure_count = 0;
        $unmodified_record_count = 0;

        $runner = new CommandRunner;
        $first_attempt_days = $runner->getFirstAttemptDays();
        $second_attempt_days = $runner->getSecondAttemptDays();

        $records = $runner->readDeclinedRecords();
        $total_record_count = count($records);

        foreach($records as $record) {
            $attempt = $runner->findDeclinedAttempt($record);
            // Check If Customer Currency Row Exists
            $customer_currency = Z1OORSHEM::where('mchorno', $record->order_number)->first();
            if($customer_currency) {
                $customer_currency = $customer_currency->load('currencyMaster');
            }
            if($attempt == null) {
                try{
                    $runner->sendFirstEmail($record, $first_attempt_days, $customer_currency);
                    $email_success_count++;
                } catch(\Exception $e) {
                    Log::error('Error Sending Email Or Updating Table ', array('error'=>$e, 'order_number'=>$record->order_number));
                    $email_failure_count++;
                }
            } elseif($attempt !== null && $attempt->attempt_number == 1) {
                if($attempt->isPastSecondAttempt($second_attempt_days)) {
                    try{
                        $runner->resendFirstEmail($record, $first_attempt_days, $customer_currency);
                        $email_success_count++;
                    } catch(\Exception $e) {
                        Log::error('Error Sending Email Or Updating Table ', array('error'=>$e, 'order_number'=>$record->order_number));
                        $email_failure_count++;
                    }
                } elseif($attempt->isPastFirstAttempt($first_attempt_days)) {
                    try{
                        $runner->sendSecondEmail($record, $first_attempt_days, $customer_currency);
                        $email_success_count++;
                    } catch(\Exception $e) {
                        Log::error('Error Sending Email Or Updating Table ', array('error'=>$e, 'order_number'=>$record->order_number));
                        $email_failure_count++;
                    }
                } else {
                    // Leave untouched until an above condition returns True
                    $unmodified_record_count++;
                }
            } elseif($attempt !== null && $attempt->attempt_number == 2) {
                // Leave untouched for other query run
                $unmodified_record_count++;
            }
        }
        $this->info('Total Running Time : '.$start_time->diffInSeconds(). ' Seconds');
        $this->info('Total Record Count : '.$total_record_count);
        $this->info('Total Success Count : '.$email_success_count);
        $this->info('Total Failure Count : '.$email_failure_count);
        $this->info('Total Unmodified Count : '.$unmodified_record_count);
        Log::info('Date - '.$start_time.' Total - '.$total_record_count.' Success - '.$email_success_count.' Failure - '.$email_failure_count.' Unmodified - '.$unmodified_record_count);
        $runner->sendDailyReportEmail($total_record_count, $email_success_count, $email_failure_count, $unmodified_record_count);

    }
}
<?php

/**
 * Created by PhpStorm.
 * User: dharrington
 * Date: 11/9/2017
 * Time: 9:36 AM
 */

namespace App\Http\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;


class DeclinedAttempt extends Model
{

    protected $connection = 'ibmi';
    protected $table = 'Z1OCCDCLN';
    protected $primaryKey = 'ccorno';
//    protected $primaryKey = null;
    public $timestamps = false;

    protected $fillable = ['ccorno','ccseq#','ccatmpt','ccadat','ccatim','ccagrp','ccagdsc','ccemail','ccamt'];

    public $appends=[
        'order_number',
        'sequence_number',
        'attempt_number',
        'date',
        'time',
        'account_group',
        'account_group_description',
        'email',
        'amount'
    ];

    public $hidden=[
        'ccorno',
        'ccseq#',
        'ccatmpt',
        'ccadat',
        'ccatim',
        'ccagrp',
        'ccagdsc',
        'ccemail',
        'ccamt'
    ];

    //---------------------------------------------
    // Scopes
    //---------------------------------------------
    public function scopeOrderNumber($query, $val) {
        return $query->where('ccorno','=', $val);
    }

    //---------------------------------------------
    // Static Methods
    //---------------------------------------------
    // todo - check what format to save date and time
    public static function add($record, $attempt) {
        DeclinedAttempt::create([
            'ccorno'  => $record->order_number,
            'ccseq#'  => $record->sequence_number,
            'ccatmpt' => $attempt,
            'ccadat'  => Carbon::now()->format('Ymd'),
            'ccatim'  => Carbon::now()->format('His'),
            'ccagrp'  => $record->account_group,
            'ccagdsc' => $record->account_group_description,
            'ccemail' => $record->text_line_1,
            'ccamt'   => $record->amount
        ]);
        return;
    }

    //---------------------------------------------
    // Accessors
    //---------------------------------------------
    protected function getOrderNumberAttribute() {
        return trim($this->attributes['ccorno']);
    }
    protected function getSequenceNumberAttribute() {
        return trim($this->attributes['ccseq#']);
    }
    protected function getAttemptNumberAttribute() {
        return trim($this->attributes['ccatmpt']);
    }
    // todo - not sure if this should be here
    protected function getDateAttribute() {
//        return trim($this->attributes['ccadat']);
        return Carbon::createFromFormat('Ymd', $this->attributes['ccadat'], 'America/New_York');
    }
    protected function getTimeAttribute() {
        return trim($this->attributes['ccatim']);
    }
    protected function getAccountGroupAttribute() {
        return trim($this->attributes['ccagrp']);
    }
    protected function getAccountGroupDescriptionAttribute() {
        return trim($this->attributes['ccagdsc']);
    }
    protected function getEmailAttribute() {
        return trim($this->attributes['ccemail']);
    }
    protected function getAmountAttribute() {
        return trim($this->attributes['ccamt']);
    }

    public function isPastFirstAttempt($days) {
        $now = Carbon::now();
        $days_past = $now->diffInDays($this->date);
        if($days_past >= $days) {
            return true;
        }
        return false;
    }
    public function isPastSecondAttempt($days) {
        $now = Carbon::now();
        $days_past = $now->diffInDays($this->date);
        if($days_past >= $days) {
            return true;
        }
        return false;
    }

}
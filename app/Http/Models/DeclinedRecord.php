<?php
/**
 * Created by PhpStorm.
 * User: dharrington
 * Date: 11/9/2017
 * Time: 9:37 AM
 */

namespace App\Http\Models;

use App\Traits\MultiCurrency;
use Illuminate\Database\Eloquent\Model;


class DeclinedRecord extends Model
{

    use MultiCurrency;

    protected $connection = 'ibmi2';
    protected $table = 'CCCHAL';
    protected $primaryKey = null;
    public $timestamps = false;
    protected $fillable = []; // read-only

    // Create all the fields for JSON data
    public $appends=[
        'order_number',
        'sequence_number',
        'order_status',
        'amount',
        'name',
        'address_line_1',
        'text_line_1',
        'account_group',
        'account_group_description',
    ];

    // Hide all the GLINX names
    public $hidden=[
        'lcorno',
        'lclseq',
        'ohords',
        'lcamou',
        'naname',
        'naadr1',
        'nfctx1',
        'ohagrp',
        'ctagrd'
    ];

    //---------------------------------------------
    // Scopes
    //---------------------------------------------

    public function scopeOrderNumber($query, $val) {
        return $query->where('lcorno', $val);
    }

    //---------------------------------------------
    // Static Methods
    //---------------------------------------------

    //---------------------------------------------
    // Methods
    //---------------------------------------------

    //---------------------------------------------
    // Accessors
    //---------------------------------------------
    protected function getOrderNumberAttribute() {
        return trim($this->attributes['lcorno']);
    }
    protected function getSequenceNumberAttribute() {
        return trim($this->attributes['lclseq']);
    }
    protected function getOrderStatusAttribute() {
        return trim($this->attributes['ohords']);
    }
    protected function getAmountAttribute() {
        return trim($this->attributes['lcamou']);
    }
    protected function getNameAttribute() {
        return trim($this->attributes['naname']);
    }
    protected function getAddressLine1Attribute() {
        return trim($this->attributes['naadr1']);
    }
    protected function getTextLine1Attribute() {
        return trim($this->attributes['nfctx1']);
    }
    protected function getAccountGroupAttribute() {
        return trim($this->attributes['ohagrp']);
    }
    protected function getAccountGroupDescriptionAttribute() {
        // remove last two characters
        return trim(substr($this->attributes['ctagrd'], 0, -2));
    }
    public function getAttemptMessage($attempt, $days) {
        switch($this->attributes['ohords']) {
            case 10:
            case 20:
            case 30:
                if($attempt == 1) {
                    return "Unfortunately, we have been unable to gain authorization on the credit or debit card provided for payment.";
                } elseif($attempt == 2) {
                    return "To date, we have not heard from you with new payment information. This is our second attempt to contact you.
                    If we have not heard from you within $days business days, your order will be cancelled.";
                }
                return "";
            case 40:
            case 45:
            case 50:
                if($attempt == 1) {
                    return "Unfortunately, at the time of final billing we were unable to gain authorization on the credit or debit card
                    provided for payment.";
                } elseif($attempt == 2) {
                    return "To date, we have not heard from you with new payment information. This is our second attempt to contact you.
                    If we have not heard from you within $days business days, the unpaid balance will be invoiced and Geiger's Accounts
                    Receivable team will be reaching out to you directly for another form of payment.";
                }
                return "";
            default:
                return "";
        }
    }
    public function amountGreaterThanZero() {
        if($this->amount > 0) {
            return true;
        }
        return false;
    }
}
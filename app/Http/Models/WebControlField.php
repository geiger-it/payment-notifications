<?php
/**
 * Created by PhpStorm.
 * User: dharrington
 * Date: 11/9/2017
 * Time: 9:38 AM
 */

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;


class WebControlField extends Model {

    protected $connection = 'ibmi';
    protected $table = 'Z1OCTLWC';
    protected $primaryKey = null;
    public $timestamps = false;
    protected $fillable = []; // read-only

    public $appends=[
        'value_1',
        'value_2'
    ];

    public $hidden=[
        'wcval1',
        'wcval2'
    ];


    //---------------------------------------------
    // Scopes
    //---------------------------------------------
    public function scopeControlValues($query) {
        $query->where('wccode','=', 'CCLTR');
    }

    //---------------------------------------------
    // Accessors
    //---------------------------------------------
    protected function getValue1Attribute() {
        return trim($this->attributes['wcval1']);
    }
    protected function getValue2Attribute() {
        return trim($this->attributes['wcval2']);
    }

}
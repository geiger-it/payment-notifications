<?php
/**
 * Created by PhpStorm.
 * User: dharrington
 * Date: 05/23/2018
 * Time: 9:27 AM
 */

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;


class Z1OCTLMC extends Model
{

    protected $connection = 'ibmi';
    protected $table = 'Z1OCTLMC';
    protected $fillable = []; // read-only

    public $incrementing = false;
    public $timestamps = false;
    public $delete = false;


    //--------------------------------------------------------------------------
    //  GETTERS
    //--------------------------------------------------------------------------
    public function getCurrencyAttribute() {
        return trim($this->attributes['zctcnum']);
    }
    public function getDescriptionAttribute() {
        return trim($this->attributes['zctcnam']);
    }
    public function getHiddenDiscountPercentageAttribute() {
        return trim($this->attributes['zcthidp']);
    }
    public function getHiddenDiscountAmountAttribute() {
        return trim($this->attributes['zcthida']);
    }
    public function getMinimumInterestTotalAttribute() {
        return trim($this->attributes['zctinmi']);
    }
    public function getMinimumInterestLineAttribute() {
        return trim($this->attributes['zctlemi']);
    }
    public function getMinimumReminderAttribute() {
        return trim($this->attributes['zctremi']);
    }
    public function getDecimalNumberForAmountAttribute() {
        return trim($this->attributes['zctdecc']);
    }
    public function getDecimalNumberForAmountDisplayAttribute() {
        return trim($this->attributes['zctdece']);
    }
    public function getDecimalNumberForPriceAttribute() {
        return trim($this->attributes['zctdcsp']);
    }
    public function getDecimalNumberForPriceDisplayAttribute() {
        return trim($this->attributes['zctdcse']);
    }
    public function getInvoiceAdjustmentAttribute() {
        return trim($this->attributes['zctivaj']);
    }
    public function getCurrencyTextNameAttribute() {
        return trim($this->attributes['zctcutn']);
    }
    public function getDateAttribute() {
        return trim($this->attributes['zctdate']);
    }
    public function getSubmittedTimeAttribute() {
        return trim($this->attributes['zcttime']);
    }
    public function getUserIdAttribute() {
        return trim($this->attributes['zctuser']);
    }

}
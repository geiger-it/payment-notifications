<?php
/**
 * Created by PhpStorm.
 * User: dharrington
 * Date: 05/23/2018
 * Time: 9:26 AM
 */

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;


class Z1OORSHEM extends Model
{

    protected $connection = 'ibmi';
    protected $table = 'Z1OORSHEM';
//    protected $primaryKey = 'mchorno';
    protected $primaryKey = 'MCHORNO';
    protected $fillable = []; // read-only

    public $incrementing = false;
    public $timestamps = false;
    public $delete = false;


    //--------------------------------------------------------------------------
    //  RELATIONSHIPS
    //--------------------------------------------------------------------------
    public function currencyMaster() {
        return $this->hasOne(Z1OCTLMC::class, 'zctcnum', 'mchccur');
    }

    //--------------------------------------------------------------------------
    //  GETTERS
    //--------------------------------------------------------------------------
    public function getCustomerCurrencyAttribute() {
        return trim($this->attributes['mchccur']);
    }
    public function getExchangeRateAttribute() {
        return trim($this->attributes['mchexr3']);
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: dharrington
 * Date: 11/9/2017
 * Time: 9:39 AM
 */

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;


class EmailDailyReport extends Mailable
{
    use Queueable, SerializesModels;

    public $total_count;
    public $success_count;
    public $failure_count;
    public $unmodified_count;
    public $date;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($total_count, $success_count, $failure_count, $unmodified_count, $date)
    {
        $this->total_count = $total_count;
        $this->success_count = $success_count;
        $this->failure_count = $failure_count;
        $this->unmodified_count = $unmodified_count;
        $this->date = $date;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('noreply@paynotifications.geiger-api.com')
            ->replyTo('cs@geiger.com')
            ->subject('Payment Notifications Daily Report')
            ->markdown('emails.email-daily-report');
    }
}
<?php

/**
 * Created by PhpStorm.
 * User: dharrington
 * Date: 11/9/2017
 * Time: 9:39 AM
 */

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;


class EmailCustomer extends Mailable
{
    use Queueable, SerializesModels;

    public $record;
    public $attempt;
    public $days;
    public $customer_currency;

    /**
     * Create a new message instance.
     *
     *
     */
    public function __construct($record, $attempt, $days, $customer_currency)
    {
        $this->record = $record;
        $this->attempt = $attempt;
        $this->days = $days;
        $this->customer_currency = $customer_currency;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('noreply@paynotifications.geiger-api.com')
            ->replyTo('cs@geiger.com')
            ->subject('Important Payment Details Needed')
            ->markdown('emails.email-customer');

    }
}
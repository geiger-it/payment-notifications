<?php
/**
 * Created by PhpStorm.
 * User: dharrington
 * Date: 11/9/2017
 * Time: 9:40 AM
 */

namespace App;

use App\Http\Models\DeclinedAttempt;
use App\Http\Models\DeclinedRecord;
use App\Http\Models\WebControlField;
use App\Mail\EmailCustomer;
use App\Mail\EmailDailyReport;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;


class CommandRunner
{
    // need for updater command
    public function readDeclinedRecords() {
        $declinedRecords = DeclinedRecord::get();
        return $declinedRecords;
    }
    // pull attempt # to find correct email to send
    public function findDeclinedAttempt($record){
        $attempt = DeclinedAttempt::where('ccorno', $record->order_number)->where('ccseq#', $record->sequence_number)->orderBy('ccadat','desc')->first();
        return $attempt;
    }
    // no previous attempt found in DeclinedAttempt table
    public function sendFirstEmail($record, $days, $customer_currency) {
        Mail::to($record->text_line_1)->send(new EmailCustomer($record, 1, $days, $customer_currency));
        DeclinedAttempt::add($record, 1);
    }
    // previous attempt is past allowed timeslot(ControlField::value1)
    public function sendSecondEmail($record, $days, $customer_currency) {
        Mail::to($record->text_line_1)->send(new EmailCustomer($record, 2, $days, $customer_currency));
        DeclinedAttempt::add($record, 2);
    }
    // previous attempt is past allowed timeslot(ControlField::value2)
    public function resendFirstEmail($record, $days, $customer_currency) {
        Mail::to($record->text_line_1)->send(new EmailCustomer($record, 1, $days, $customer_currency));
        DeclinedAttempt::add($record, 1);
    }
    // finished laravel command, daily update to verify job still running
    public function sendDailyReportEmail($total_count, $success_count, $failure_count, $unmodified_count) {
        $users = config('Custom.mailto');
        foreach($users as $user) {
            Mail::to($user)->send(new EmailDailyReport($total_count, $success_count, $failure_count, $unmodified_count, Carbon::today()->toDateString()));
        }
    }
    // need for comparison to today's date 
    public function getFirstAttemptDays() {
        $attempt_days = WebControlField::controlValues()->first();
        return $attempt_days->value_1;
    }
    // need for comparison to today's date 
    public function getSecondAttemptDays() {
        $attempt_days = WebControlField::controlValues()->first();
        return $attempt_days->value_2;
    }

}
@component('mail::message')
    SO #{{ $record->order_number  }}<br><br>
    Dear Customer,<br><br>
    Thank you for your recent {{ $record->account_group_description }} store order.<br><br>
    {{ $record->getAttemptMessage($attempt, $days) }}<br><br>
    @if($record->amountGreaterThanZero())
        @if($customer_currency)
            Your order total, including tax and shipping, is {{ $record->customerCurrency($record->amount, $customer_currency) }} {{ $record->customerCurrencyName($customer_currency) }}.<br><br>
        @else
            Your order total, including tax and shipping, is ${{ number_format($record->amount, 2, '.', '') }}.<br><br>
        @endif
    @endif
    Please contact us at 1-888-343-4437 to provide payment information. Note that for<br>
    security reasons, we do not accept credit card information via email.<br><br>
    Thank you for your prompt attention to this matter, we look forward to hearing from you<br>
    at your earliest opportunity.<br><br>
    Thank you,<br><br>
    <img src="{{ asset('images/geiger_logo_pdf.png') }}" alt="Geiger" title="Geiger" style="display: block;" width="126" height="46"><br>
    70 Mt Hope Ave • Lewiston, ME 04240<br>
    888.343.4437 • <a href="https://www.geiger.com">www.geiger.com</a><br>
    <span style="font-weight: bold;">#geigergetsit  • <span style="color: red;">Corporate Programs</span> • Hours: 8 a.m. - 5 p.m. (Eastern)</span>
@endcomponent

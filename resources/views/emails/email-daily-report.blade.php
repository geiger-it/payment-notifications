@component('mail::message')
    Daily Report For : {{ $date }}<br>
    @component('mail::table')
        | Total | Success | Failure | Unmodified |
        |:-----:|:-------:|:-------:|:----------:|
        | {{ $total_count }} | {{ $success_count }} | {{ $failure_count }} | {{ $unmodified_count }} |
    @endcomponent
@endcomponent